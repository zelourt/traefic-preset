## Install by script
```bash
$ curl -s -L https://gitlab.com/zelourt/traefic-preset/-/raw/master/script.sh | bash
```

## Setup traefik

```bash
$ docker network create web --attachable
$ docker compose up --build -d
```

## Middlewares

```bash
$ echo $(htpasswd -nb user password) | sed -e s/\\$/\\$\\$/g
```

#### In labels:
```
- "traefik.http.middlewares.whoami-auth.basicauth.users=test:$$apr1$$ra8uoeq5$$HqiATqC5edVVEXznsNiVV/,test2:$$apr1$$8ol2akty$$BW.Fsa.K3tc1DzcJ6l9ql1"
- "traefik.http.routers.whoami.middlewares=whoami-auth"
```

## Setup docker (myservie)
```
- "traefik.enable=true"
- "traefik.http.routers.myservie.rule=Host(`domain.net`)"
- "traefik.http.routers.myservie.entrypoints=web,websecure"
- "traefik.http.routers.myservie.tls.certresolver=myresolver"
- "traefik.http.services.myservie-service.loadbalancer.server.port=80"
```

## Gen passwords
```bash
$ sudo apt install apache2-utils
$ echo $(htpasswd -nbB <USER> "<PASS>") | sed -e s/\\$/\\$\\$/g
```
